#include <iostream>
#include "Point.hpp"

int Point::comp = 0;

Point::Point(){
	x = y = 0;
	incComp();
	std::cout << "Nouveau Point !" << std::endl;
}

Point::Point(int a, int b){
	x = a, y = b;
	incComp();
	std::cout << "Nouveau Point !" << std::endl;
}

Point::~Point(){
	x = y = 0;
	decComp();
	std::cout << "Point detruit !" << std::endl;
}

int Point::getComp(void){
	return comp;
}
int Point::getX(void){
	return x;
}
int Point::getY(void){
	return y;
}

void Point::setX(int n){
	x = n;
}
void Point::setY(int n){
	y = n;
}

void Point::deplacerDe(int vx, int vy){
	x += vx;
	y += vy;
}
void Point::deplacerVers(int vx, int vy){
	x = vx;
	y = vy;
}

void Point::incComp(){
	++comp;
}

void Point::decComp(){
	--comp;
}