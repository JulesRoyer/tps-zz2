#ifndef CLASS_POINT

#define CLASS_POINT

class Point {
	private:
		int x;
		int y;
		static int comp;

	public:
		Point();
		Point(int a, int b);
		int getX(void);
		int getY(void);

		void setX(int n);
		void setY(int n);

		void incComp();
		int getComp();
		void decComp();

		void deplacerDe(int vx, int vy);
		void deplacerVers(int vx, int vy);
		~Point();
};


#endif