#include <iostream>
#include "Point.hpp"

void echange(int * a, int * b){
	int c = *b;
	*b = *a;
	*a = c;
}

void echange2(int &a, int &b){
	int c = b;
	b = a;
	a = c;
}

//1
/*int main(int argc, char ** argv)
{
  for(int i=0; i< 120; ++i)
    std::cout << "Bonjour les ZZ" << 2 << std::endl;

  return 0;
}*/

//2
/*#include <string>

int main(int, char **)
{
  std::string prenom; // type spécial pour les chaînes de caractères
  int age;

  std::cout << "Quel est votre prénom ?" << std::endl;
  std::cin >> prenom;
  std::cout << "Quel est votre age ?" << std::endl;
  std::cin >> age ;
  std::cout << "Bonjour "<< prenom << std::endl;

  return 0;
}*/
//3
/*int main(int, char**){
	Point p;
	std::cout << p.getComp() << std::endl;
	Point p2(10, 9);
	std::cout << p.getComp() << std::endl;
	std::cout << p2.getComp() << std::endl;
	Point * p3 = new Point(40, 6);
	std::cout << p.getComp() << std::endl;
	std::cout << p2.getComp() << std::endl;
	std::cout << p3->getComp() << std::endl;
	int x;
	std::cout << "Combien vaut x ?" << std::endl;
	std::cin >> x ;
	p.setX(x);
	std::cout << p.getX() << std::endl;
	std::cout << p.getX() << std::endl;
	std::cout << p2.getX() << std::endl;
	std::cout << p3->getX() << std::endl;

	delete p3;
	std::cout << p.getComp() << std::endl;
	std::cout << p2.getComp() << std::endl;
	std::cout << p3->getComp() << std::endl;
	return 0;
}
*/
/*
void fonction1(int a)
{
  std::cout << &a << std::endl;
}

void fonction2(int& a)
{
  std::cout << &a << std::endl;
}

int main(int, char **)
{
  int age = 41;

  std::cout << &age << std::endl;
  fonction1(age);
  fonction2(age);

  return 0;
}*/

int main(int, char**){
	int a = 5;
	int b = 8;
	echange(&a, &b);
	std::cout << a << " ; " << b << std::endl;

	echange2(a, b);
	std::cout << a << " ; " << b << std::endl;
}



