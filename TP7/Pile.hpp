#ifndef PILE_H
#define PILE_H

#include <iostream>

template<typename T>
class Pile{
public:
	Pile(int taille = 100);
	Pile(const Pile&);
	~Pile();

	Pile& operator=(const Pile&);

	bool empty(void) const; // check
	const T& top(void) const; // check
	void push(const T&); // check
	void pop(void); // check
	int size(void) const; // check
private:
	T * pile;
	int tailleP;
	int curseur;
};

#include "Pile.cpp"

#endif