template<typename T>
bool Pile<T>::empty(void) const{
	return this->curseur == 0;
}

template<typename T>
const T& Pile<T>::top(void) const{
	if(this->empty()){
		throw std::out_of_range("Pile vide, pas de top() possible");
	}
	return this->pile[this->curseur-1];
}

template<typename T>
void Pile<T>::push(const T& x){
	if(this->curseur >= tailleP) {
		throw std::out_of_range("Pile pleine, pas de push() possible");
	}
	else {
		this->pile[this->curseur] = x;
		++this->curseur;
	}
}

template<typename T>
void Pile<T>::pop(void){
	if(this->empty()){
		throw std::invalid_argument("Pile vide, pas de pop() possible");
	}
	else {
		--this->curseur;
	}
}

template<typename T>
int Pile<T>::size() const{
	return curseur;
}

template<typename T>
Pile<T>::Pile(int taille){
	this->pile = new T[taille];
	this->curseur = 0;
	this->tailleP = taille;
}

template<typename T>
Pile<T>::~Pile(){
	delete [] pile;
	this->curseur = 0;
	this->tailleP = 0;
}