#include "ZZ.hpp"

using namespace std;

ZZ::ZZ(string familyname, string name, double n) : nom(familyname), prenom(name), note(n)
{}

ZZ::~ZZ(){}

double ZZ::getNote() const {
	return this->note;
}

bool ZZ::operator<(const ZZ& zed) const {
	return ( (this->nom + this->prenom) < (zed.getNom() + zed.getPrenom()) );//this->note < zed.getNote();
}

const string & ZZ::getNom() const {
	return this->nom;
}

const string & ZZ::getPrenom() const {
	return this->prenom;
}

ostream& operator<<(ostream & os, const ZZ& zed) {
	os << zed.getNom() << " " << zed.getPrenom() << endl;
	return os;
}

ZZ & ZZ::operator=(const ZZ & zed) {
	this->nom = zed.getNom();
	this->prenom = zed.getPrenom();
	this->note = zed.getNote();
	return *this;
}