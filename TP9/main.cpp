#include <algorithm>
#include <vector>
#include <queue>
#include <iostream>
#include <iterator>
#include <cstdlib>
#include <ctime>
#include <numeric>
#include "ZZ.hpp"

using namespace std;

class CompareNote {
	public:
	bool operator()(const ZZ & zz1, const ZZ & zz2) const {
		return zz1.getNote() < zz2.getNote();
	}
};

class Sequence {
protected:
	int nb;
public:
	Sequence(int n=0) : nb(n){}
	~Sequence(){}
	int operator()(void){ return nb++; }
	
};

class Aleatoire
{
public:
	Aleatoire(){
		srand(time(nullptr));
	}
	~Aleatoire(){}
	int operator()(void){ return rand()%101; }
	
};



/*typedef std::vector<ZZ>  vzz;
// OU en C++ 2011
// using vzz = std::vector<ZZ> ;

vzz zz;

// il faut mettre des elements
// zz.push_back(ZZ(...));

priority_queue<ZZ> tri;
priority_queue<ZZ, vzz, CompareNote> tri2;*/

int main (int, char **) {
	/*ZZ zed("Royer", "Jules", 10);
	zz.push_back(zed);

	ZZ zed2("Perrin", "Loic", 20);
	zz.push_back(zed2);

	for(vzz::iterator it = zz.begin(); it!=zz.end(); ++it) {
		tri.push(*it);
		tri2.push(*it);
	}

	cout << "-----V1-----" << endl;

	vector<int> v1(50); Sequence s;
	generate(v1.begin(), v1.end(), s);
	for(vector<int>::iterator it = v1.begin(); it!=v1.end(); ++it) {
	   cout << *it << endl;
	}


	cout << "-----V2-----" << endl;
	vector<int> v2;
	generate_n(back_inserter(v2), 50, Sequence());
	for(vector<int>::iterator it = v2.begin(); it!=v2.end(); ++it) {
	   cout << *it << endl;
	} */

	cout << "-----V1-----" << endl;

	vector<int> v1(50); Aleatoire s;
	generate(v1.begin(), v1.end(), s);
	for(vector<int>::iterator it = v1.begin(); it!=v1.end(); ++it) {
	   cout << *it << endl;
	}


	cout << "-----V2-----" << endl;
	vector<int> v2;
	generate_n(back_inserter(v2), 50, Aleatoire());
	for(vector<int>::iterator it = v2.begin(); it!=v2.end(); ++it) {
	   cout << *it << endl;
	}

	cout << "-----Moyenne V1-----" << endl;
	int somme = accumulate(v1.begin(), v1.end(), 0);
	double moyenne = somme / double(v1.size());
	cout << moyenne << endl;

	cout << "-----Min V1-----" << endl;
	cout << *min_element(v1.begin(), v1.end()) << endl;
	return 0;
}