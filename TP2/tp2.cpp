#include <iostream>
#include <cstdlib>

//---------- Bavarde --
class Bavarde {
	public:
		Bavarde(int x = 0);
		~Bavarde();
		int get();
		void afficher();
	private:
		static int compteur;
		int n;
} bizarre(1);

Bavarde::Bavarde(int x){
	this->n = x;
	std::cout << "Construction de " << this->n << std::endl;	
}

Bavarde::~Bavarde(){
	std::cout << "Tais-toi " << this->n << std::endl;
}

int Bavarde::get(){
	return this->n;
}

void Bavarde::afficher(){
	std::cout << "Affichage de " << this->n << std::endl;
}





//---------- Couple --
class Couple {
	public:
		Couple(int n = 0, int a = 0, int b = 0);
		~Couple();
	private:
		int x;
		Bavarde B1;
		Bavarde B2;
};

Couple::Couple(int n, int a, int b): B1(a), B2(b){
	this->x = n;
	std::cout << "Creation de couple " << this->x << std::endl;
}

Couple::~Couple(){
	std::cout << "Destruction de couple " << this->x << std::endl;
}





//---------- Famille --
class Famille {
	public:
		Famille(int taille = 0);
		~Famille();
	private:
		Bavarde * tableau;
		int tailleF;
};

Famille::Famille(int taille){
	this->tailleF = taille;
	this->tableau = new Bavarde[this->tailleF];
	std::cout << "Nouvelle famille de taille " << this->tailleF << std::endl;
}

Famille::~Famille(){
	delete [] this->tableau;
	std::cout << "Famille de taille " << this->tailleF << " detruite" << std::endl;
}





Bavarde globale(2);

void fonction(Bavarde b){
	std::cout << "code de la fonction" << std::endl;
}

int main(int, char**){
	//Bavarde b;
	//Bavarde * p = new Bavarde(3);

	//fonction(b);

	//delete p;

	//std::cout << Bavarde(0).get() << std::endl;
	//return 0;

	//const int TAILLE = 20;
	// Bavarde   tab1[TAILLE];
	// Bavarde * tab2 = new Bavarde[TAILLE];

	// for (int i =0; i < TAILLE; ++i)
	// {
	// 	tab1[i].afficher();
	// 	tab2[i].afficher();
	// }

	// delete [] tab2;


	// Couple couple1(45, 12, 13);

	// Famille unefamille(12);

	// Bavarde * bav = (Bavarde *)malloc(sizeof(Bavarde));
	// free(bav);

	return 0;
}



