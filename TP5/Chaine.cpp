#include "Chaine.hpp"

Chaine::Chaine(){
	this->capa = -1;
	this->tableau = NULL;
}

Chaine::Chaine(int taille){
	if(taille>0){
		this->capa = taille;
		this->tableau = new char[this->capa];
		this->tableau[0] = '\0';
	} else {
		this->capa = -1;
		this->tableau = NULL;
	}
}

Chaine::Chaine(const char * s){
	strcpy((this->tableau = new char[(this->capa = strlen(s))]), s); //Sale
}

Chaine::Chaine(const Chaine & s){
	strcpy((this->tableau = new char[(this->capa = s.getCapacite())]), s.c_str()); //Sale
}

Chaine::~Chaine(){
	delete [] this->tableau;
}

Chaine & Chaine::operator= (const Chaine & une){
	if(une.getCapacite() == this->getCapacite()){
		strcpy(this->tableau, une.c_str());
	} else {
		delete [] this->tableau;
		strcpy((this->tableau = new char[(this->capa = une.getCapacite())]), une.c_str()); //Sale
	}
	return *this;
}

Chaine & Chaine::operator= (const char * une){
	int taille = strlen(une);
	if(taille == this->getCapacite()){
		strcpy(this->tableau, une);
	} else {
		delete [] this->tableau;
		strcpy((this->tableau = new char[(this->capa = taille)]), une); //Sale
	}
	return *this;
}

char Chaine::operator[](int i){
	char val;
	if(i<0) throw std::out_of_range("Indice < 0 :( ");
	else if(i>this->capa) throw std::bad
}

void Chaine::remplacer(const char * s){

}

void Chaine::afficher(void) const{
	std::cout << this->tableau << std::endl;
}

void Chaine::afficher(std::stringstream & ss) const{
	ss << this->c_str() /*<< std::endl*/;
}

const char * Chaine::c_str(void) const{
	return this->tableau;
}

int Chaine::getCapacite() const{
	return this->capa;
}

