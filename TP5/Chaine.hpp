#ifndef CPP5__CHAINE_HPP
#define CPP5__CHAINE_HPP

#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>

class Chaine  {
private:
	int capa;
	char * tableau;
public:
	Chaine();
	Chaine(int taille);
	Chaine(const char * s);
	Chaine(const Chaine & s);
	~Chaine();
	Chaine & operator= (const Chaine & une);
	Chaine & operator= (const char * une);
	char operator[](int i);
	void remplacer(const char * s);
	void afficher(void) const;
	void afficher(std::stringstream ss) const;
	const char * c_str(void) const;
	int getCapacite() const;

};

std::ostream & operator<<(std::ostream &, const Chaine &);

#endif
