#ifndef PILE_CLASS
#define PILE_CLASS

#include <iostream>

class Pile{
protected:
	int taille;
	int sommet;
	int * tab;
public:
	Pile(int x=0);
	Pile(const Pile &);
	~Pile();
	Pile & operator=(const Pile &);
	int size() const;
	int top() const;
	void pop();
	void push(int);
	bool empty() const;
};

#endif