#include "ZZ.hpp"

using namespace std;

ZZ::ZZ(string familyname, string name, double n) : nom(familyname), prenom(name), note(n)
{}

ZZ::~ZZ(){}

double ZZ::getNote() const {
	return this->note;
}

bool ZZ::operator<(const ZZ& zed) const {
	return this->note < zed.getNote();
}

string getNom() const {
	return this->nom;
}

string getPrenom() const {
	return this->prenom;
}

ostream& operator<<(ostream & os, const ZZ& zed) {
	os << zed.getNom() << " " << zed.getPrenom() << endl;
}

/*
typedef std::vector<ZZ>  vzz;
// OU en C++ 2011
// using vzz = std::vector<ZZ> ;

vzz zz;

// il faut mettre des elements
// zz.push_back(ZZ(...));

priority_queue<ZZ> tri;

for(vzz::iterator it = zz.begin();
    it!=zz.end(); ++it)
 tri.push(*it);

while(!tri.empty()) {
   cout << tri.top() << " ";
   tri.pop();
}*/

int main(int argc, char ** argv){

}