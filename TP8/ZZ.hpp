#ifndef ZZ_H
#define ZZ_H

#include <string>
#include <iostream>

using namespace std;

class ZZ {
private:
	string nom, prenom;
	double note;
public:
	ZZ(string familyname="Dupont", string name="Jean", double n=10);
	~ZZ();
	double getNote() const;
	string getNom() const;
	string getPrenom() const;
	bool operator<(const ZZ&) const;
};

ostream& operator<<(ostream & os, const ZZ&);

#endif