#include <iostream>
#include <string>
#include <sstream>
#include "A.hpp"
#include "B.hpp"

using namespace std;

string A::toString() {
    stringstream oss;
    oss << "A " << val;
    return oss.str();
};

A::A() {
    cout << "Instance " << this->toString() << endl;
}

A::A(int const & val):val(val) {
    cout << "Instance " << this->toString() << endl;
}

A::~A() {
    cout << "Tais-toi " << this->toString() << endl;
}

void A::send(B* b) {
    b->exec(val);
};

void A::exec(int val) {
    this->val += val;
};


