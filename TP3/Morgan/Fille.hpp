#ifndef Fille_H
#define Fille_H
#include <string>
#include "Mere.hpp"
#include "Point.hpp"

class Fille : public Mere {
public:
    Fille();
    ~Fille();
    std::string toString();
};

#endif
