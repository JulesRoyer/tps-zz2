#ifndef Mere_H
#define Mere_H
#include <string>
#include "Point.hpp"

class Mere {
public:
    Mere();
    ~Mere();
    std::string toString();
};

#endif
