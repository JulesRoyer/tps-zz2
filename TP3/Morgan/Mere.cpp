#include <iostream>
#include <string>
#include <sstream>
#include "Mere.hpp"

using namespace std;

string Mere::toString() {
    stringstream oss;
    oss << "Mere";
    return oss.str();
};

Mere::Mere() {
    cout << "Instance " << this->toString() << endl;
}

Mere::~Mere() {
    cout << "Tais-toi " << this->toString() << endl;
}




