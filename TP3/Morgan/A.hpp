#include <string>

class B;
class A {
    int val;
public:
    A();
    A(int const &);
    ~A();
    std::string toString();
    void send(B*);
    void exec(int);
};
