#include "Point.hpp"

using namespace std;

Point::Point():x(0), y(0) {
}

Point::Point(const int & xinit, const int & yinit):x(xinit), y(yinit) {
}

int Point::getX() const {
  return x;
}

void Point::setX(const int & entry) {
  this->x = entry;
}

int Point::getY() const {
  return y;
}

void Point::setY(const int & entry) {
  this->y = entry;
}


