#ifndef Point_H
#define Point_H

class Point {
    int x;
    int y;

public:
    Point();
    Point(const int &, const int &);
    int getX() const;
    int getY() const;
    void setX(const int &);
    void setY(const int &);
};



#endif
