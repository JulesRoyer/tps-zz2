#include <iostream>
#include <string>
#include <sstream>
#include "B.hpp"
#include "A.hpp"

using namespace std;

string B::toString() {
    stringstream oss;
    oss << "B " << val;
    return oss.str();
};

B::B() {
    cout << "Instance " << this->toString() << endl;
}

B::B(int const & val):val(val) {
    cout << "Instance " << this->toString() << endl;
}

B::~B() {
    cout << "Tais-toi " << this->toString() << endl;
}

void B::send(A* a) {
    a->exec(val);
};

void B::exec(int val) {
    this->val += val;
};



