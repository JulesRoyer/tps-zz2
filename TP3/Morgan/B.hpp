#include <string>

class A;
class B {
    int val;
public:
    B();
    B(int const &);
    ~B();
    std::string toString();
    void send(A*);
    void exec(int);
};
