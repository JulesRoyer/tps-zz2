#include <iostream>
#include <string>
#include <sstream>
#include "Fille.hpp"

using namespace std;

string Fille::toString() {
    stringstream oss;
    oss << "Fille";
    return oss.str();
};

Fille::Fille() {
    cout << "Instance " << this->toString() << endl;
}

Fille::~Fille() {
    cout << "Tais-toi " << this->toString() << endl;
}




