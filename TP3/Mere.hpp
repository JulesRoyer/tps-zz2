#ifndef __CLASS_MERE__
#define __CLASS_MERE__

#include <string>
#include <iostream>

using namespace std;

class Mere {
	public:
		Mere();
		Mere(string n);
		~Mere();
		int getCpt();
		string getNom();
	protected:
		static int cpt;
		string nom;
};

#endif