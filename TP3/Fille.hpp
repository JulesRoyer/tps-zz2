#ifndef __CLASS_FILLE__
#define __CLASS_FILLE__

#include "Mere.hpp"

using namespace std;

class Fille : public Mere {
	public:
		Fille();
		Fille(string n);
		~Fille();
	private:

};

#endif