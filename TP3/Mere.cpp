#include "Mere.hpp"

using namespace std;

int Mere::cpt = 0;

int Mere::getCpt() {
	return cpt;
}

Mere::Mere(){
	cout << "Construction nouvelle Mere, une de plus sur " << cpt << endl;
	++cpt;
}

Mere::Mere(string n){
	this->nom = n;
	cout << "Construction nouvelle Mere " << this-> nom <<", une de plus sur " << cpt << endl;
	++cpt;
}

Mere::~Mere(){
	cout << "Destruction Mere" << endl;
	--cpt;
}