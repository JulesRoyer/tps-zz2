#ifndef __RECTANGLE_CLASS__
#define __RECTANGLE_CLASS__

#include <string>
#include "Point.hpp"

using namespace std;

#define max(A, B) (((A) > (B)) ? (A) : (B))
#define min(A, B) (((A) < (B)) ? (A) : (B))

class Rectangle {
	public:
		Rectangle(const Point & pt, const int longueur, const int hauteur);
		Rectangle(const Point & un, const Point & deux);
		~Rectangle();
		string toString();
	protected:
		int x, y, w, h;
};



#endif