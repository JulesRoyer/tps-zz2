#include <iostream>
#include <string>
#include <sstream>

#include "Cercle.hpp"

using namespace std;

Cercle::Cercle(Point pt, int rayon) : Rectangle(pt, 2*rayon, 2*rayon){

}

Cercle::Cercle(Point pt, int w, int h) : Rectangle(pt, w, h){

}

string Cercle::toString(){
	stringstream buff;
	buff << "Cercle de centre x = " << this->x << ", y = " << this->y << ", hauteur = " << this->h << " et longueur = " << this->w;
	return buff.str(); 
}