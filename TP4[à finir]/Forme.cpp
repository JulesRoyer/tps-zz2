#include "Forme.hpp"

using namespace std;

int Forme::proId = 0;
int Forme::nbFormes = 0;

Forme::Forme() : pt(), w(0), h(0), Id(proId){
	incId();
	incNb();
}

Forme::~Forme(){
	decNb();
}

void Forme::decNb(){
	--nbFormes;
}

void Forme::incNb(){
	++nbFormes;
}

void Forme::incId(){
	++proId;
}

int Forme::getId(){
	return this->Id;
}

int Forme::prochainId(){
	return proId;
}