#ifndef __CLASS_POINT__
#define __CLASS_POINT__

class Point {
	private:
		int x;
		int y;
		static int comp;
		void incComp();
		void decComp();

	public:
		Point();
		Point(int a, int b);
		int getX(void) const;
		int getY(void) const;

		void setX(int n);
		void setY(int n);

		int getComp();

		void deplacerDe(int vx, int vy);
		void deplacerVers(int vx, int vy);
		~Point();
};

extern Point ORIGINE;


#endif