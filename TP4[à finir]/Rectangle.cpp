#include <iostream>
#include <string>
#include <sstream>
#include "Rectangle.hpp"

using namespace std;

Rectangle::Rectangle(const Point & un, const Point & deux){
	this->x = un.getX();
	this->y = un.getY();
	this->w = max(x, deux.getX()) - min(x, deux.getX());
	this->h = max(y, deux.getY()) - min(y, deux.getY());
}

Rectangle::Rectangle(const Point & pt, const int longueur, const int hauteur){
	this->x = pt.getX();
	this->y = pt.getY();
	this->w = longueur;
	this->h = hauteur;
}

Rectangle::~Rectangle(){
	std::cout << "Destruction" << std::endl;
}

string Rectangle::toString(){
	stringstream buff;
	buff << "Rectangle x = " << this->x << ", y = " << this->y << ", longueur = " << this->w << ", hauteur = " << this->h ;
	return buff.str();
}