#ifndef __FORME_CLASS__
#define __FORME_CLASS__

#include "Point.hpp"

using namespace std;

enum class COULEURS {
  NOIR, BLANC, BLEU, JAUNE, ROUGE, 
};

class Forme {
	public:
		Forme();
		~Forme();

		Point getPoint();
		int getHaut();
		int getLarg();
		int getNb();
		int getId();
		COULEURS getCouleur();

		void setCouleur();
		void setX();
		void setY();

		static int prochainId();

	protected:
		Point pt;
		int w;
		int h;
		int Id;

		static int nbFormes;
		static int proId;

		void incNb();
		void incId();
		void decNb();
		COULEURS couleur;
};



#endif