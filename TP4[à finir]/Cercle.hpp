#ifndef __CERCLE_CLASS__
#define __CERCLE_CLASS__

#include "Rectangle.hpp"
#include <string>


using namespace std;

class Cercle : public Rectangle {
	public:
		Cercle(Point pt, int rayon);
		Cercle(Point pt, int w, int h);
		string toString();
};

#endif